#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "md5.h"

const int PASS_LEN=20;        // Maximum any password can be
const int HASH_LEN=33;        // Length of MD5 hash strings

// Given a hash and a plaintext guess, return 1 if
// the hash of the guess matches the given hash.
// That is, return 1 if the guess is correct.
int tryguess(char *hash, char *guess)
{
    // Hash the guess using MD5
    //char *cracked = md5(guess, strlen(guess));
    // Compare the two hashes
    if(strcmp(guess, hash) == 0) return 1;
    // Free any mallocd memory
    return 0;
}

// Read in a file and return the array of strings.
// Use the technique we showed in class to expand the
// array as you go.
char **readfile(char *filename)
{
    FILE *f = fopen(filename, "r");
    if(!f)
    {
        printf("Can't open that file\n");
        exit(1);
    }
    
    int size = 50;
    
    char **strs = (char**)malloc(size * sizeof(char*));
    
    char str[50];
    int i = 0;
    while(fscanf(f, "%s\n", str) != EOF)
    {
        if(i == size - 1)
        {
            size+=10;
            char **newarr = (char**)realloc(strs, size * sizeof(char*));
            if(newarr != NULL) strs = newarr;
            else exit(1);
        }
        char *newstr = (char*)malloc(strlen(str)*sizeof(char));
        strcpy(newstr, str);
        strs[i] = newstr;
        i++;
    }
    strs[i] = NULL;
    return strs;
}


int main(int argc, char *argv[])
{
    if (argc < 3) 
    {
        printf("Usage: %s hash_file dict_file\n", argv[0]);
        exit(1);
    }

    // Read the hash file into an array of strings
    char **hashes = readfile(argv[1]);

    // Read the dictionary file into an array of strings
    char **dict = readfile(argv[2]);
    char **dictOriginal = readfile(argv[2]);

    // For each hash, try every entry in the dictionary.
    // Print the matching dictionary entry.
    // Need two nested loops.
    int counter=0;
    
    while(dict[counter]!=NULL)
    {
        dict[counter] = md5(dict[counter], strlen(dict[counter]));
        counter++;
    }
    
    int i=0;
    int j=0;
    while(hashes[i]!=NULL)
    {
        while(1)
        {
            if(tryguess(hashes[i], dict[j]) == 1)
            {
                printf("%s %s\n", dict[j], dictOriginal[j]);
                break;
            }
            if(hashes[j] == NULL) break;
            j++;
        }
        j=0;
        i++;
    } 
}
